-- | Module      : SaxCombinatorsSpec
--   Description : Combinators for lazy stream parsing with SAX events
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module SaxCombinatorsSpec (main, spec) where

import Data.Bifunctor          (first)
import Data.List               (partition)
import Data.Text               (Text, pack)
import Test.Hspec

import Text.XML.SAXCombinators

-- import Import.Debug


main :: IO ()
main = hspec spec


spec :: Spec
spec = do
  describe "Event functions can" $ do
    it "toElement - convert to Element" $ do
      let (h:t) = events
      case toElement (h,t) of
        Left e         -> expectationFailure e
        Right (e,rest) -> do
          (e, getEvs rest) `shouldBe`
            ((1,NElement "mainTag" [] parseLoc
               [(2,NText "mainText")
               ,(3,NElement "subTag" [] parseLoc
                  [(4,NText "subText")
                  ,(5,NComment "This is a comment")
                  ,(6,NText "is")
                  ,(7,NText "\t")
                  ,(8,NText "first")
                  ]
                )
               ]
             )
            ,[TagEnd "mainTag",TagStart "nextTag" []]
            )

    it "spanChildrenE - extract children of named tag" $ do
      let (cs, rest) = spanChildrenE "mainTag" events
      (getEvs cs, getEvs rest) `shouldBe`
         ([TagStart "mainTag" []
            , Textual "mainText"
            , TagStart "subTag" []
              , Textual "subText"
              , Comment "This is a comment"
              , Textual "is",Textual "\t",Textual "first"
            , TagEnd "subTag"
            ]
         ,[ TagEnd "mainTag", TagStart "nextTag" []])

    it "spanUntilTagE - extract events until named next tag" $ do
      let (cs, rest) = spanUntilTagE "nextTag" $ drop 1 events
      (getEvs cs, getEvs rest) `shouldBe`
        ([ Textual "mainText"
         , TagStart "subTag" []
           , Textual "subText"
           , Comment "This is a comment"
           , Textual "is", Textual "\t", Textual "first"
         , TagEnd "subTag"
         , TagEnd "mainTag"
         ]
        ,[TagStart "nextTag" []])

    it "spanUntilTagE - not extract events until named next tag, if such tag is first in the list" $ do
      let (cs, rest) = spanUntilTagE "mainTag" events
      (getEvs cs, getEvs rest) `shouldBe`
        ([],
         [ TagStart "mainTag" []
           , Textual "mainText"
           , TagStart "subTag" []
             , Textual "subText"
             , Comment "This is a comment"
             , Textual "is", Textual "\t", Textual "first"
           , TagEnd "subTag"
         , TagEnd "mainTag"
         , TagStart "nextTag" []
         ])

    it "spanConcatTextE - concat first sequential text events and insert delimiter" $ do
      spanConcatTextE "*" txtEvents `shouldBe` ("Number*  *one*\n*is*\t*first",[])

    it "spanConcatTextE - not concat first sequential text events and insert delimiter, if first is not text event" $ do
      let es = (100, Comment "", parseLoc) : txtEvents
      spanConcatTextE "*" es `shouldBe` ("",es)

    it "spanConcatTextE, clearBlanksE - clear only whitespace text events" $ do
      spanConcatTextE " " (clearBlanksE txtEvents) `shouldBe` ("Number one is first", [])

    it "clearBlanksE - clear only whitespace text events" $ do
      getEvs (clearBlanksE txtEvents) `shouldBe`
        [Textual "Number",Textual "one",Textual "is",Textual "first"]

  describe "Node functions can" $ do
    it "clearBlanksN - clear only whitespace text nodes" $ do
      clearBlanksN idxNodes `shouldBe`
        [ (1,NElement "aTag" [] parseLoc [])
        , (2,NText "subText")
        , (3,NComment "This is a comment")
        , (4,NComment "  ")
        , (5,NText "is")
        , (7,NText "first")
        ]

    it "spanConcatTextN - concat text nodes and insert delimiter" $ do
      let ns = [ (1, NText "is")
               , (2, NText "\t")
               , (3, NText "first")
               , (4, NElement "aTag" [] parseLoc [])
               , (5, NText "subText")
               , (6, NComment "This is a comment")
               , (7, NComment "  ")
               ]

      spanConcatTextN "*" ns `shouldBe`
        ( "is*\t*first"
        , [ (4,NElement "aTag" [] parseLoc [])
          , (5,NText "subText")
          , (6,NComment "This is a comment")
          , (7,NComment "  ")]
        )
    it "spanConcatTextN - not concat text nodes and insert delimiter, if first is not text node" $ do
      let ns = [ (1, NComment "This is a comment")
               , (2, NText "is")
               , (3, NText "\t")
               , (4, NText "first")
               , (5, NElement "aTag" [] parseLoc [])
               , (6, NText "subText")
               , (7, NComment "  ")
               ]
      spanConcatTextN "*" ns `shouldBe` ( "", ns)

  describe "Stream processing of" $ do
    it "events, result & state" $ do
      let parser (e, es, s) = do
            let (d, s') = case snd3 e of
                            TagStart n _ -> ("S-" <> n, s <> [n])
                            Textual t    -> ("T-" <> t, s <> [t])
                            Comment t    -> ("C-" <> t, s <> [t])
                            TagEnd n     -> ("E-" <> n, s <> [n])
                            x            -> (tshow x ,  s <> ["unknown"])
            Right ([d], drop 2 es, s')
      processEvents parser ["init state"] events `shouldBe`
        ( [             "S-mainTag","T-subText","T-\t","E-mainTag"]
        , ["init state",  "mainTag",  "subText",  "\t",  "mainTag"]
        )

    it "leftovers, result & state" $ do
      let parser (tag, nodes, s) = do
            let (s',rest) = first (map (\(i,NComment t) -> tshow i <> ". " <> t))
                              $ partition (\case (_,NComment _) -> True; _ -> False) nodes
            Right ((tag,s'), rest, s <> ["Gathered comments"])

      let downTag = (123, "Upstream record", False) :: (Int,Text,Bool)
      processNodes parser ([(downTag, idxNodes)], ["init state" :: Text])
        `shouldBe`
        ( [(
              ( downTag
              , [ "3. This is a comment"
                , "4.   "
                ]
              )
           , [ (1, NElement "aTag" [] parseLoc [])
             , (2, NText "subText")
             , (5, NText "is")
             , (6, NText "\t")
             , (7, NText "first")
             ]
           )
          ]
        , [ "init state"
          , "Gathered comments"
          ]
        )


-- | -------------------------------------------------------------------------------------
-- Data for testing
------------------------------------------------------------------------------------------

txtEvents :: [EventL]
txtEvents =
  let strings = ["Number", "  ", "one", "\n", "is", "\t", "first"]
    in map (\(n,t) -> (n, Textual t, parseLoc)) $ zip [1..] strings

events :: [EventL]
events =
  let evs = [ TagStart "mainTag" []
              , Textual "mainText"
              , TagStart "subTag" []
                , Textual "subText"
                , Comment "This is a comment"
              , Textual "is",Textual "\t",Textual "first"
              , TagEnd "subTag"
            , TagEnd "mainTag"
            , TagStart "nextTag" []
            ]
  in map (\(n,e) -> (n, e, parseLoc)) $ zip [1..] evs

idxNodes :: [IdxNode]
idxNodes = [ (1, NElement "aTag" [] parseLoc [])
           , (2, NText "subText")
           , (3, NComment "This is a comment")
           , (4, NComment "  ")
           , (5, NText "is")
           , (6, NText "\t")
           , (7, NText "first")
           ]


parseLoc :: ParseLocation
parseLoc = ParseLocation
  { plLine = 1
  , plColumn  = 1
  , plByteIndex = 2
  , plByteCount = 3
  }

tshow :: Show a => a -> Text
tshow = pack . show

snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x

getEvs :: [EventL] -> [Event]
getEvs = map snd3
