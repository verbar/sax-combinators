# SAX combinators

Few combinators for dealing with SAX events, like extracting children events of opening tag, converting to datatype,
lazy parsing of SAX events converting them to some datatype, lazy parsing leftovers from SAX events parsing, etc.
