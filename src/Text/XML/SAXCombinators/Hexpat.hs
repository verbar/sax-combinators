-- | Module      : Text.XML.SAXCombinators.Hexpat
--   Description : Combinators for lazy stream parsing SAX events from Hexpat library
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Text.XML.SAXCombinators.Hexpat
  ( )
where

import Data.Text
import Prelude

import Text.XML.Expat.SAX      as H (SAXEvent (..), XMLParseError (..),
                                     XMLParseLocation (..))
import Text.XML.SAXCombinators as SC


------------------------------------------------------------------------------------------
-- SaxCombinatorsBackend instance
------------------------------------------------------------------------------------------

-- | [Hexpat] (https://hackage.haskell.org/package/hexpat) instance for
--   'SaxCombinatorsBackend'
instance SaxCombinatorsBackend (SAXEvent Text Text) XMLParseLocation where
  toEvent     = toSPEvent
  toIdxEventL = toEventL

-- | [Hexpat's XMLParseLocation instance ] (https://hackage.haskell.org/package/hexpat)
--   instance for  'ShowParseLocation'
instance ShowParseLocation XMLParseLocation where
  showParseLocation = pack . show

-- | Converter of 'SAXEvent' to 'Event'
toSPEvent :: SAXEvent Text Text -> Event
toSPEvent = \case
  XMLDeclaration a b c             -> XmlDeclaration a b c
  StartElement n as                -> TagStart n as
  EndElement n                     -> TagEnd n
  StartCData                       -> CDataStart
  EndCData                         -> CDataEnd
  ProcessingInstruction a b        -> ProcInstruction  a b
  H.Comment t                      -> SC.Comment t
  CharacterData t                  -> Textual t
  FailDocument (XMLParseError t l) -> Fail (pack $ show t) (toLocation l)

-- | Converter of 'XMLParseLocation' to 'ParseLocation'
toLocation :: XMLParseLocation -> ParseLocation
toLocation XMLParseLocation{..} =
  ParseLocation xmlLineNumber xmlColumnNumber xmlByteIndex xmlByteCount

-- | Converter of indexed 'SAXEvent' with location to 'EventL'
toEventL :: (Int, (SAXEvent Text Text, XMLParseLocation)) -> EventL
toEventL (idx,(e,l)) = (idx, toEvent e, toLocation l)
