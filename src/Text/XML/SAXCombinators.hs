-- | Module      : Text.XML.SAXCombinators
--   Description : Combinators for lazy stream parsing SAX events
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Text.XML.SAXCombinators
  ( -- * Combinators for parsing SAX events

    -- $generalInfo

    -- ** Datatypes
    Event(..)
  , Node(..)
  , ParseLocation (..)

  -- ** Classes for extending with other backends
  , ShowParseLocation (..)
  , SaxCombinatorsBackend (..)

  -- ** Parsing stream of Events or Nodes
  , processEvents
  , processNodes

  -- ** Conversions
  -- , toNode
  , toElement

  -- ** Using Nodes and Events
  , spanChildrenE
  , spanUntilTagE
  , spanConcatTextN
  , spanConcatTextE
  , clearBlanksE
  , clearBlanksN

  -- ** Node getters & predicates
  , getName
  , getText
  , getAttrs
  , getLocation
  , getChildren
  , isNamed
  , isText

  -- ** Event getters & predicates
  , getTextE
  , isTextE

    -- ** Type aliases
  , Attrs
  , DeclEnc
  , DeclVersion
  , EventL
  , IdxNode
  , TagName

  )
where

import Prelude                   hiding (concat)

import Control.Arrow             (first)
import Control.Monad.Error.Class (MonadError, throwError)
import Data.Bool                 (bool)
import Data.Int                  (Int64)
import Data.Store                (Store)
import Data.String               (IsString)
import Data.Text                 (Text, pack, strip, intercalate)
import GHC.Generics              (Generic)

-- import Import.Debug

{- $generalInfo
@SAX combinators@ provide some tools to make it easier to lazily parse SAX events produced
by backends. Currently only [Hexpat] (https://hackage.haskell.org/package/hexpat) backend
is implemented.

Example from tmx-saxparser


Stream parsing:

@
streamTmxUnkTo :: (Default a, SaxCombinatorsBackend event location, MonadIO m)
               => (TmxState a -> (TmxUnknown,[IdxNode]) -> m (Either PError (TmxState a)))
               -- ^ output function
               -> Text
               -- ^ title of TMX file
               -> UniqTmxId
               -- ^ unique identifier
               -> [(event, location)]
               -- ^ lazy list of events with their location in source file
               -> m (Either PError (TmxState a))
streamTmxUnkTo output title uId =
  uncurry (doOutput output)
  -- ^ send data to some storage, one-by-one
    . parseUnknownTags
    -- ^ there are some unknown tags, so let's parse them as well with 'processNodes'
    . processEvents (parseTmx title uId) (defGlb, def)
    -- ^ parse TMX with only the known tags, defined by standard
    . zipWith (curry toIdxEventL) [1..]
    -- ^ convert events to 'IdxNode' and tuple with ordinal
@

Tag parsing:

@
mkTmx :: TmxParser m
      => Text -> UniqTmxId -> EventL -> [EventL] -> m (TmxCore, [IdxNode],Globals)
mkTmx title uniqId e (clearBlanksE -> es) = do
  let (cs,_rest) = spanUntilTagE "body" es
  toElement (e,cs) >>= \case
    (n@(_,NElement _ as _ cs'), []) -> do
      (hdr,oth,g) <- parseTag n "header" mkHeaderTag cs'
      tmx \<- TmxCore title uniqId \<$> checkVer n as \<*> pure hdr
      pure (tmx,oth,g)
    _ -> throwError $ WrongTag "tmx" (Left "tmx")
  where
    checkVer n as = reqAttr n "version" as >>= \case
      "1.4"  -> pure "1.4"
      v      -> mkErr n $ BadVersion (v, "1.4")
@
-}


------------------------------------------------------------------------------------------
-- Classes
------------------------------------------------------------------------------------------

-- | Two methods which link Sax combinators with backend
class SaxCombinatorsBackend event location | event -> location where
  toEvent     :: event                    -> Event
  toIdxEventL :: (Int, (event, location)) -> EventL

-- | Defining how is parsing location displayed to user
class ShowParseLocation location where
  showParseLocation :: location -> Text


------------------------------------------------------------------------------------------
-- Aliases
------------------------------------------------------------------------------------------
-- | 'Event' with index and parse location
type EventL = (Int, Event, ParseLocation)
-- | List of XML attributes
type Attrs       = [(Text, Text)]
-- | Name of the tag
type TagName     = Text
-- | Version in XML declaration
type DeclVersion = Text
-- | Encoding in XML declaration
type DeclEnc     = Text
-- | 'Node' with index
type IdxNode     = (Int,Node)


------------------------------------------------------------------------------------------
-- Event
------------------------------------------------------------------------------------------

-- | Base Event type
data Event =
    XmlDeclaration  DeclVersion (Maybe DeclEnc) (Maybe Bool) -- ^ XML header declaration
  | TagStart        TagName Attrs                            -- ^ Start tag event
  | TagEnd          TagName                                  -- ^ End tag event
  | CDataStart                                               -- ^ Start CDData section
  | CDataEnd                                                 -- ^ End CDData section
  | ProcInstruction Text Text                                -- ^ Processing instructions
  | Comment         Text                                     -- ^ Comment
  | Textual         Text                                     -- ^ free text
  | Fail            Text ParseLocation                       -- ^ parsing failed at position
  deriving (Eq, Show, Generic)
instance Store Event

-- | Get text of event, or blank if it is not an 'Textual'
getTextE :: EventL -> Text
getTextE (_, Textual t, _) = t
getTextE _                 = ""

-- | Check if 'Node' is 'NText'
isTextE :: EventL -> Bool
isTextE (_, Textual _, _) = True
isTextE _                 = False

------------------------------------------------------------------------------------------
-- Location of the event
------------------------------------------------------------------------------------------

-- | Location of the event, as reported by low-level SAX parser
data ParseLocation = ParseLocation
  { plLine      :: Int64
  , plColumn    :: Int64
  , plByteIndex :: Int64
  , plByteCount :: Int64
  } deriving (Show, Eq, Ord, Generic)
instance Store             ParseLocation
instance ShowParseLocation ParseLocation where
  showParseLocation ParseLocation{..} = pack $
       " @ line#: "            <> show plLine
    <> ", level#: "            <> show plColumn
    <> ", bytes# from start: " <> show plByteIndex

------------------------------------------------------------------------------------------
-- Node
------------------------------------------------------------------------------------------
-- | List of events can be transformed into Node which contains more suitable shape for processing
data Node =
    NElement         !Text !Attrs ParseLocation [IdxNode] -- ^ named node with children and attrs
  | NText            !Text                                -- ^ free text
  | NCData           !Text                                -- ^ CDData
  | NComment         !Text                                -- ^ comment
  | NProcInstruction !Text !Text                          -- ^ processing instructions
  | NFail            !Text !ParseLocation                 -- ^ a failure
  deriving (Show, Eq, Ord)

-- | Convert 'EventL' to indexed 'Node'. Used internally
toNode :: EventL -> Maybe IdxNode
toNode (idx, e, l) = case e of
  XmlDeclaration {}    -> Nothing
  TagStart n as        -> Just (idx, NElement n as l [])
  TagEnd _             -> Nothing
  CDataStart           -> Nothing -- TODO make CData Node
  CDataEnd             -> Nothing
  ProcInstruction  a b -> Just (idx, NProcInstruction a b)
  Comment t            -> Just (idx, NComment t)
  Textual t            -> Just (idx, NText t)
  -- Fail _ _             -> Nothing
  Fail t fl            -> Just (idx, NFail t fl)

-- | Get name of node, or blank if it is not an 'NElement'
getName :: Node -> Text
getName (NElement n _ _ _) = n
getName  _                 = ""

-- | Get text of node, or blank if it is not an 'NText'
getText :: Node -> Text
getText (NText t) = t
getText _         = ""

-- | Get list of attributes of the node, or empty if it is not an 'NElement'
getAttrs :: Node -> Attrs
getAttrs (NElement _ as _ _) = as
getAttrs _                   = []

-- | Get location of node in document, or Nothing if it is not an 'NElement'
getLocation :: Node -> Maybe ParseLocation
getLocation (NElement _ _ l _) = Just l
getLocation _                  = Nothing

-- | Get a list of children of the node, or empty if it is not an 'NElement'
getChildren :: Node -> [IdxNode]
getChildren (NElement _ _ _ cs) = cs
getChildren _                   = []

-- | Check if element has specified name. Always False if it is not an 'NElement'
isNamed :: Text -> Node -> Bool
isNamed nm (NElement n _ _ _) = n == nm
isNamed _ _                   = False

-- | Check if 'Node' is 'NText'
isText :: Node -> Bool
isText (NText _) = True
isText _         = False


------------------------------------------------------------------------------------------
-- Event and Node management
------------------------------------------------------------------------------------------

-- | Convert events to a 'Node' and return indexed node with the rest of events
--   The 'TagEnd' event of specified 'TagStart' event is the first of the rest.
toElement :: (IsString er, MonadError er m) => (EventL, [EventL]) -> m (IdxNode, [EventL])
toElement ((idx,TagStart nm as, l), events) = do
  let (cs, rest) = spanChildrenE nm events
  (, rest) . (idx,) . NElement nm as l <$> go cs
  where
    go (e@(_,TagStart n _,_) : rest) = do
      let (cs,csRest) = spanChildrenE n rest
      toElement (e,cs) >>= \case
        (y,[]) -> (y :) <$> go csRest
        _      ->           go csRest
    go (e:es) = maybe (go es) (\n -> (n :) <$> go es) $ toNode e
    go []     = pure []
toElement _ = throwError "Must be `TagStart` event"

-- | Split events to children of named tag and the rest.
--   The 'TagEnd' event is not included in the children. It is the first of the rest.
spanChildrenE :: TagName -> [EventL] -> ([EventL], [EventL])
spanChildrenE parent = span ((/=) (TagEnd parent) . snd3)

-- | Split events to events before the named tag and after
--   First 'Event' in the list should not be 'TagStart' named as endTag
spanUntilTagE :: TagName -> [EventL] -> ([EventL], [EventL])
spanUntilTagE endTag = span (\case (_, TagStart nm _, _) -> endTag /= nm
                                   _                     -> True)

-- | Intercalate all textual events with delimiter and merge, return merged text and non-text events
spanConcatTextE :: Text -> [EventL] -> (Text, [EventL])
spanConcatTextE delim = first (intercalate delim . map getTextE) . span isTextE

-- | Intercalate only first sequential text nodes with delimiter and merge.
--   Return merged text and non-text nodes.
spanConcatTextN :: Text -> [IdxNode] -> (Text, [IdxNode])
spanConcatTextN delim =
  first (intercalate delim . map (getText . snd)) . span (isText . snd)


-- | Remove all text events that are only whitespace
clearBlanksE :: [EventL] -> [EventL]
clearBlanksE = foldr (\x -> (<>) (bool [x] [] (isTextE x && isBlank (getTextE x)))) []

-- | Remove all text nodes that are only whitespace
clearBlanksN :: [IdxNode] -> [IdxNode]
clearBlanksN =
  foldr (\x -> (<>) (bool [x] [] (isText (snd x) && isBlank (getText $ snd x))))  []


------------------------------------------------------------------------------------------
-- Streaming parser
------------------------------------------------------------------------------------------

-- | Lazily process a stream of elements 1-by-1. Supplied parser can process more than 1
--   event and return the rest of unprocessed 'Event's.
processEvents :: ((eventL, [eventL], s) -> Either (tag,s) ([tag], [eventL], s))
              -> s
              -> [eventL]
              -> ([tag],s)
processEvents parser s (e:es) = case parser (e, es, s) of
  Left  (ts     , s') -> ([ts], s')
  Right (ts, es', s') -> let (ts', s'') = processEvents parser s' es'
                          in (ts <> ts', s'')
processEvents _ s [] = ([], s)


-- | Lazily process a list of indexed 'Node's 1-by-1. Supplied parser can process more
--   than 1 indexed node and return the rest of unprocessed indexed 'Node's.
processNodes :: (( tag,    [IdxNode],   s) -> Either a (supTag, [IdxNode], s))
             -> ([(tag,    [IdxNode])], s)
             -> ([(supTag, [IdxNode])], s)
processNodes parser ((tag,unused):ts, s) = case parser (tag, unused, s) of
  Left  _err          -> ([], s)
  Right (u, unks, s') -> let (ts', s'') = processNodes parser (ts, s')
                          in ((u,unks) : ts', s'')
processNodes _ ([], s) = ([],s)



------------------------------------------------------------------------------------------
-- Utilities
------------------------------------------------------------------------------------------

-- | Second of triple
{-# INLINE snd3 #-}
snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x


isBlank :: Text -> Bool
isBlank t = strip t == ""
